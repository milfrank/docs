=======================================
Install Raspbian on Raspiberry pi 3 B+
=======================================

If you want to use a Raspiberry pi 3 B+ as a desktop, here is how to install Raspbian.

First download and write image to minicard. You can refer to (https://www.raspberrypi.org/documentation/installation/installing-images/)

- Desktop (LXDE)

| sudo apt-get install --no-install-recommends xserver-xorg
| sudo apt-get install --no-install-recommends xinit
| sudo apt-get install lxde-core lxappearance
| sudo  apt-get install lightdm

-  sudo no password

| $ sudo passwd  # set root password
| $ su root  # login as root
| $ visudo
| //  add this line to the end
| pi ALL=(ALL) NOPASSWD:ALL
| //  disable root login
| $ sudo passwd -l root

- Auto login desktop

| // Edit /etc/lightdm/lightdm.conf
| // Set autologin-user=pi
| $ sudo reboot

- Applications

| $ sudo apt install curl unzip git
| $ sudo apt install gedit


- install chromium browser

| sudo apt-get update
| wget -qO - http://bintray.com/user/downloadSubjectPublicKey?username=bintray | sudo apt-key add -
| echo "deb http://dl.bintray.com/kusti8/chromium-rpi jessie main" | sudo tee -a /etc/apt/sources.list
| sudo apt-get update
| sudo apt-get install chromium-browser


Refer to:

https://raspberrypi.stackexchange.com/questions/41603/installing-chrome-on-raspbian

.. note::  Raspberry Pi is good at being a work computer, it is cheap,  it also had limit rsource with 1 GB RAM, for example, I run text editor, Web browser,SSH, you can't play game with it.

