========================
Create an AWS Account
========================

First of all, before you create an AWS account, you have to realize what that mean to your business, it will potentially impact the way you run business. After you make your decision, to create an AWS account, you can follow AWS documentation.

https://aws.amazon.com/premiumsupport/knowledge-center/create-and-activate-aws-account/

When select a support plan, select 'Basic'