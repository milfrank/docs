==================================
Install docker on Ubuntu Linux
==================================

docker had a very clear tutorial on How to Install docker on Ubuntu Linux https://docs.docker.com/install/linux/docker-ce/ubuntu/, assume you have created an AWS account, you decide to deloy your application on a cloud server, which run Ubuntu Linux, you have logged on into SSH with a terminal. Here are brief you go:

.. code-block:: html

    # Uninstall old versions
    $ sudo apt-get remove docker docker-engine docker.io containerd runc

    $ sudo apt-get update

    $ sudo apt-get install \
        apt-transport-https \
        ca-certificates \
        curl \
        gnupg-agent \
        software-properties-common

    # Add Docker’s official GPG key:
    $ curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

    $ sudo add-apt-repository \
     "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
    $(lsb_release -cs) \
    stable"

    $ sudo apt-get update

    # Install the latest version of Docker CE and containerd
    $ sudo apt-get install docker-ce docker-ce-cli containerd.io

    # commands without sudo
    $ sudo usermod -aG docker $USER

    Log out and login docker to take the changes effect

    # Check docker version
    $ docker version

Alternatively, you can install docker by a script, here is it:

.. code-block:: html

    #!/bin/sh

    set -eu

    # Docker
    sudo apt remove --yes docker docker-engine docker.io \
    && sudo apt-get update \
    && sudo apt-get --yes --no-install-recommends install \
        apt-transport-https \
        ca-certificates \
    && wget --quiet --output-document=- https://download.docker.com/linux/ubuntu/gpg \
        | sudo apt-key add - \
    && sudo add-apt-repository \
        "deb [arch=$(dpkg --print-architecture)] https://download.docker.com/linux/ubuntu \
        $(lsb_release --codename --short) \
        stable" \
    && sudo apt-get update \
    && sudo apt-get --yes --no-install-recommends install docker-ce \
    && sudo usermod -aG docker $USER \
    && sudo systemctl enable docker \
    && printf '\nDocker installed successfully\n\n'

    printf 'Waiting for Docker to start...\n\n'
    sleep 3
    

First save scripts 'docker.sh' in a directory, then in that directory,run

$ sudo chmod +x docker.sh

# Run the script 

$ sudo ./docker.sh

Note: 1. Don't use apt update, use apt-get,may encounter 'Hash Sum Mismatch' error,solved by 

$ apt-get update -o Acquire::CompressionTypes::Order::=gz

Refer to:

https://blog.packagecloud.io/eng/2016/03/21/apt-hash-sum-mismatch/