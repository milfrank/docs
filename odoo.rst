==========================
How to run Odoo Container
==========================

1. Pull Images

``$ docker pull postgres:10``

https://hub.docker.com/_/postgres

``$ docker pull odoo:12.0``

https://hub.docker.com/_/odoo

2. Run Odoo containers

1）Run postgres container

.. code:: rst

  docker run -d --name odoodb \
    --restart=always \
    -e POSTGRES_USER=odoo \
    -e POSTGRES_PASSWORD=odoo \
    -e POSTGRES_DB=postgres \
    postgres:10
- --restart=always, container restart automatically after server restart.

2）Run odoo container

.. code:: rst

  docker run -p 9876:8069 --name odooapp \
    --restart=always \
    -v $(pwd)/odoo_addons:/mnt/extra-addons  \
    --link odoodb:db \
    -t odoo:12.0
- port number 9876, odoo port number, change to what you like.
- $(pwd)/odoo_addons:/mnt/extra-addons, customized addons directory.

3）Access odoo

Open your browser,http://IP:portnumber/ to visit  Odoo