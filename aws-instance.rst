======================
Create a cloud server
======================

After you create an AWS account, you can launch a sever (AWS call this instance) by clicking your mouse,

1. sever type
2. operating system alone with server type, select 'Ubuntu Linux'
3. download pair key file. Use with it for SSH login.

You have to configure 2 points

1. Elastic IP

Prevent IP change when restarting. Follow the tutorial:

https://docs.aws.amazon.com/vpc/latest/userguide/vpc-eips.html


2. Security Groups

Allow port 22 for SSH on inbond.
